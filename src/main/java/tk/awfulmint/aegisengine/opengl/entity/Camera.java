package tk.awfulmint.aegisengine.opengl.entity;

import org.joml.Vector3f;
import tk.awfulmint.aegisengine.AEGISEngine;
import tk.awfulmint.aegisengine.opengl.shader.StaticShader;

/**
 * Essa classe representa uma camera.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class Camera {
    private Vector3f position, rotation;
    private StaticShader staticShader;

    /**
     * Cria uma instancia de camera.
     *
     * @param position      Posição inicial da camera.
     * @param rotation      Rotação Inicial da camera.
     * @param staticShader  StaticShader.
     */
    public Camera(Vector3f position, Vector3f rotation, StaticShader staticShader) {
        this.position = position;
        this.rotation = rotation;
        this.staticShader = staticShader;

        staticShader.useShader();
        staticShader.setupProjectionMatrix(AEGISEngine.winWidth, AEGISEngine.winHeight, 75.0f, 0.01f, 1000.0f);
    }

    /**
     * Atualiza a posição da entidade nos 3 eixos de translaçao.
     * Deve ser atualizada antes de atualizar a world matrix.
     *
     * @param x
     * @param y
     * @param z
     */
    public void updatePos(float x, float y, float z) {
        position.x -= x;
        position.y -= y;
        position.z -= z;
    }

    /**
     * Atualiza a posição da entidade nos 3 eixos de rotação.
     * Deve ser atualizada antes de atualizar a world matrix.
     *
     * @param x
     * @param y
     * @param z
     */
    public void updateRotation(float x, float y, float z) {
        rotation.x -= x;
        rotation.y -= y;
        rotation.z -= z;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(Vector3f rotation) {
        this.rotation = rotation;
    }
}
