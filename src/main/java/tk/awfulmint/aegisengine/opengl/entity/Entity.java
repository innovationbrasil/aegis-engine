package tk.awfulmint.aegisengine.opengl.entity;

import org.joml.Vector3f;
import tk.awfulmint.aegisengine.opengl.model.TexturedModel;

/**
 * Esta classe representa uma entidade, uma entidade é a
 * evoluçao de um TexturedModel. Uma entidade conta com
 * o poder das matrizes, ou seja, você pode mover,
 * rotacionar, escalonar uma entidade mais facilmente do
 * que você poderia fazer com um TexturedModel.
 *
 * @author PCXDO
 * @version v0.1
 * @since  v0.1
 */
public class Entity {
    private TexturedModel texturedModel;
    private Vector3f position, rotation;
    private float scale;

    /**
     * Construtor principal da classe, que é utilizado em EntityLoader.
     *
     * @param texturedModel Objeto com o VAO e outras informações.
     * @param position      Posição inicial no espaço 3D.
     * @param rotation      Rotação inicial no espaço 3D.
     * @param scale         Escala incial no espaço 3D.
     */
    public Entity(TexturedModel texturedModel, Vector3f position, Vector3f rotation, float scale) {
        this.texturedModel = texturedModel;
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }

    /**
     * Atualiza a posição da entidade nos 3 eixos de translaçao.
     * Deve ser atualizada antes de atualizar a world matrix.
     *
     * @param x
     * @param y
     * @param z
     */
    public void updatePos(float x, float y, float z) {
        position.x += x;
        position.y += y;
        position.z += z;
    }

    /**
     * Atualiza a posição da entidade nos 3 eixos de rotação.
     * Deve ser atualizada antes de atualizar a world matrix.
     *
     * @param x
     * @param y
     * @param z
     */
    public void updateRotation(float x, float y, float z) {
        rotation.x += x;
        rotation.y += y;
        rotation.z += z;
    }

    public TexturedModel getTexturedModel() {
        return texturedModel;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public Vector3f getRotation() {
        return rotation;
    }

    public void setRotation(Vector3f rotation) {
        this.rotation = rotation;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
}
