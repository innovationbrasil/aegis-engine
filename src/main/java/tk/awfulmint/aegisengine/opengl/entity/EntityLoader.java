package tk.awfulmint.aegisengine.opengl.entity;

import org.joml.Vector3f;
import tk.awfulmint.aegisengine.opengl.model.TexturedModel;
import tk.awfulmint.aegisengine.opengl.shader.StaticShader;

/**
 * Esta classe tem como objetivo carregar todas as
 * entidades do jogo.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class EntityLoader {

    /**
     * Carrega uma entidade pronta para renderização.
     *
     * @param texturedModel Modelo base texturizado.
     * @return  Uma entidade.
     */
    public Entity loadEntity(TexturedModel texturedModel) {
        return new Entity(texturedModel, new Vector3f(0, 0, -1.5f), new Vector3f(0,0,0), 1);
    }

    /**
     * Carrega uma entidade do tipo Camera.
     *
     * @param initialPos    Posição inicial da camera.
     * @param initialRot    Rotação inicial da camera.
     * @param staticShader  StaticShader
     * @return  Uma camera.
     */
    public Camera loadCamera(Vector3f initialPos, Vector3f initialRot, StaticShader staticShader) {
        return new Camera(initialPos, initialRot, staticShader);
    }

}
