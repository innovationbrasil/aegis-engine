package tk.awfulmint.aegisengine.opengl.texture;

import static org.lwjgl.opengl.GL11.*;

/**
 * Esta classe representa uma textura de modelo e é usada
 * para bindar a textura no objeto corretamente.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class ModelTexture {
    private int textureID, width, height;

    /**
     * Cria uma textura de modelo.
     *
     * @param textureID ID da textura.
     * @param width     Largura da textura.
     * @param height    Altura da textura.
     */
    public ModelTexture(int textureID, int width, int height) {
        this.textureID = textureID;
        this.width = width;
        this.height = height;
    }

    /**
     * Binda a textura no renderer.
     */
    public void bindTexture() {
        glBindTexture(GL_TEXTURE_2D, textureID);
    }

    /**
     * Desbinda a textura.
     */
    public void unbindTexture() {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public int getTextureID() {
        return textureID;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
