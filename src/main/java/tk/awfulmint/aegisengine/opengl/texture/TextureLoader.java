package tk.awfulmint.aegisengine.opengl.texture;

import org.lwjgl.system.MemoryStack;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.stb.STBImage.*;

/**
 * Carregador de textura.
 *
 * @author PCXDO
 * @since v0.1
 * @version v0.1
 */
public final class TextureLoader {

    /**
     * Carrega uma textura de modelo.
     *
     * @param path  Caminho para o arquivo da textura.
     * @return Retorna uma textura de modelo.
     */
    public static ModelTexture loadTexture(String path) {
        int id, width, height;

        id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, id);

        ByteBuffer img;
        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer w = stack.mallocInt(1);
            IntBuffer h = stack.mallocInt(1);
            IntBuffer c = stack.mallocInt(1);

            img = stbi_load(path, w, h, c, 4);
            if(img == null) throw new RuntimeException("Nao foi possivel carregar uma textura!" +
                    System.lineSeparator() + stbi_failure_reason());

            width = w.get(0);
            height = h.get(0);
        }

        setTexParam(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        setTexParam(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        setTexParam(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        setTexParam(GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);
        glBindTexture(GL_TEXTURE_2D, 0);

        return new ModelTexture(id, width, height);
    }

    private static void setTexParam(int name, int value) {
        glTexParameteri(GL_TEXTURE_2D, name, value);
    }

}
