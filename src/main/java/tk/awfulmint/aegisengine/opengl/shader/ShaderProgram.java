package tk.awfulmint.aegisengine.opengl.shader;

import static org.lwjgl.opengl.GL20.*;

/**
 * Esta classe representa uma ShaderProgram, contem
 * os 2 Shaders padrões.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class ShaderProgram {
    private Shader vShader, fShader;
    private int programID;

    /**
     * Cria um novo ShaderProgram.
     *
     * @param vShader   Vertex Shader.
     * @param fShader   Fragment Shader.
     * @return Um ShaderProgram.
     */
    public ShaderProgram createProgram(Shader vShader, Shader fShader) {
        ShaderProgram sp = new ShaderProgram();
        sp.vShader = vShader;
        sp.fShader = fShader;

        sp.programID = glCreateProgram();
        glAttachShader(sp.programID, sp.vShader.getShaderID());
        glAttachShader(sp.programID, sp.fShader.getShaderID());

        glLinkProgram(sp.programID);
        if(glGetProgrami(sp.programID, GL_LINK_STATUS) == 0) {
            throw new RuntimeException("Erro enquanto linkando o ShaderProgram! Error message:\n" + glGetProgramInfoLog(sp.programID, 2048));
        }

        glValidateProgram(sp.programID);
        if(glGetProgrami(sp.programID, GL_VALIDATE_STATUS) == 0) {
            System.err.println("Atenção! Possiveis erros na validação do ShaderProgram! Error message:\n" + glGetProgramInfoLog(sp.programID, 2048));
        }

        return sp;
    }

    /**
     * Binda o ShaderProgram para ser utilizado pelo OpenGL.
     */
    public void bind() {
        glUseProgram(programID);
    }

    /**
     * Desbinda o ShaderProgram.
     */
    public void unbind() {
        glUseProgram(0);
    }

    /**
     * Limpa a memoria.
     */
    public void clean() {
        unbind();
        glDetachShader(programID, vShader.getShaderID());
        glDetachShader(programID, fShader.getShaderID());

        glDeleteShader(vShader.getShaderID());
        glDeleteShader(fShader.getShaderID());

        glDeleteProgram(programID);
    }

    public Shader getvShader() {
        return vShader;
    }

    public Shader getfShader() {
        return fShader;
    }

    public int getProgramID() {
        return programID;
    }

}
