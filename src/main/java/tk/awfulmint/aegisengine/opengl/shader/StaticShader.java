package tk.awfulmint.aegisengine.opengl.shader;

import org.joml.Matrix4f;
import tk.awfulmint.aegisengine.opengl.entity.Camera;
import tk.awfulmint.aegisengine.opengl.entity.Entity;

import static org.lwjgl.opengl.GL20.*;

/**
 * StaticShader é o shader padrao da engine.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class StaticShader extends AbstractShader {
    private static final String VSHADER_PATH = "data/shaders/vShader.glsl";
    private static final String FSHADER_PATH = "data/shaders/fShader.glsl";

    private Matrix4f projMatrix;

    private int FS_RENDER_MODE;
    private int FS_TEX_SAMPLER;
    private int VS_PROJ_MATRIX;
    private int VS_MODEL_MATRIX;
    private int VS_VIEW_MATRIX;

    public StaticShader() {}

    /**
     * Cria um novo StaticShader.
     *
     * @param vShader   Vertex Shader
     * @param fShader   Fragment Shader
     */
    private StaticShader(Shader vShader, Shader fShader) {
        super(vShader, fShader);
    }

    @Override
    public void getAllUniformLocations() {
        FS_RENDER_MODE = getUniformLocation("renderMode");
        FS_TEX_SAMPLER = getUniformLocation("texSampler");
        VS_PROJ_MATRIX = getUniformLocation("projMatrix");
        VS_MODEL_MATRIX = getUniformLocation("modelMatrix");
        VS_VIEW_MATRIX = getUniformLocation("viewMatrix");
    }

    /**
     * Seta a matrix de projeção. Deve ser chamada apenas uma vez antes
     * do loop de renderização.
     */
    public void setupProjectionMatrix(float winWidth, float winHeight, float FOV, float Z_NEAR, float Z_FAR) {
        float aspectRatio = winWidth / winHeight;
        projMatrix = new Matrix4f().perspective(FOV, aspectRatio, Z_NEAR, Z_FAR);

        setUniform(VS_PROJ_MATRIX, projMatrix);
    }

    /**
     * Seta a matrix de visualização. Deve ser chamada antes de renderizar
     * qualquer objeto. É utilizada na camera.
     *
     * @param viewMatrix Matriz de Visualiuzação.
     */
    public void setupViewMatrix(Matrix4f viewMatrix) {
        setUniform(VS_VIEW_MATRIX, viewMatrix);
    }

    /**
     * Seta a matriz de modelo. É utilizada por todos os modelos.
     *
     * @param modelMatrix Matriz de Modelo.
     */
    public void setupModelMatrix(Matrix4f modelMatrix) {
        setUniform(VS_MODEL_MATRIX, modelMatrix);
    }

    /**
     * Cria uma matriz de vizualização.
     *
     * @param camera Camera principal.
     * @return Uma matriz de visualização.
     */
    public Matrix4f getViewMatrix(Camera camera) {
        Matrix4f viewMatrix = new Matrix4f();
        viewMatrix.rotateX((float) Math.toRadians(camera.getRotation().x))
            .rotateY((float) Math.toRadians(camera.getRotation().y))
            .rotateZ((float) Math.toRadians(camera.getRotation().z));

        viewMatrix.translate(-camera.getPosition().x, -camera.getPosition().y, -camera.getPosition().z);
        return viewMatrix;
    }

    /**
     * Cria uma matriz de modelo.
     *
     * @param entity Entidade na qual a matriz vai ser baseada.
     * @return  Uma matriz de modelo.
     */
    public Matrix4f getModelMatrix(Entity entity) {
        Matrix4f modelMatrix = new Matrix4f();
        modelMatrix.rotateX((float) Math.toRadians(entity.getRotation().x))
                .rotateY((float) Math.toRadians(entity.getRotation().y))
                .rotateZ((float) Math.toRadians(entity.getRotation().z));

        modelMatrix.translate(entity.getPosition());
        modelMatrix.scale(entity.getScale());

        return modelMatrix;
    }

    /**
     * Altera o modo de renderização do Fragment Shader.
     *
     * renderMode == 0 -> Color Mode
     * renderMode == 1 -> Texture Mode
     * renderMode == 2 -> Color + Texture Mode
     *
     * @param renderMode Modo de renderização.
     */
    public void setRenderMode(int renderMode) {
        setUniform(FS_RENDER_MODE, renderMode);
    }

    public void setTextureSampler(int texPos) {
        setUniform(FS_TEX_SAMPLER, texPos);
    }

    /**
     * Cria um novo StaticShader.
     *
     * @return
     */
    public StaticShader createNew() {
        Shader vShader = ShaderLoader.loadShader(VSHADER_PATH, GL_VERTEX_SHADER);
        Shader fShader = ShaderLoader.loadShader(FSHADER_PATH, GL_FRAGMENT_SHADER);

        return new StaticShader(vShader, fShader);
    }

}
