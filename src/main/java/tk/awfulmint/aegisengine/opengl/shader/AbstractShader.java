package tk.awfulmint.aegisengine.opengl.shader;

import org.joml.*;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

/**
 * Esta classe serve como base para todos os outros Shaders.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public abstract class AbstractShader {
    private ShaderProgram shaderProgram;

    /**
     * Carrega as localizações de todas as variaveis uniformes.
     */
    public abstract void getAllUniformLocations();

    /**
     * Cria um novo AbstractShader.
     *
     * @param vShader   VertexShader
     * @param fShader   FragmentShader
     */
    public AbstractShader(Shader vShader, Shader fShader) {
        shaderProgram = new ShaderProgram().createProgram(vShader, fShader);
        getAllUniformLocations();
    }

    protected AbstractShader() {}

    /**
     * Carrega o ID da variavel uniforme.
     *
     * @param uniformName   Nome da variavel uniforme.
     * @return  Retorna o ID da variavel uniforme.
     */
    public int getUniformLocation(String uniformName) {
        return glGetUniformLocation(shaderProgram.getProgramID(), uniformName);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param value     Novo valor da variavel.
     */
    public void setUniform(int uniform, int value) {
        useShader();
        glUniform1i(uniform, value);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, float data) {
        useShader();
        glUniform1f(uniform, data);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, boolean data) {
        if(data) {
            useShader();
            glUniform1i(uniform, 1);
        } else {
            useShader();
            glUniform1i(uniform, 0);
        }
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, Vector2i data) {
        useShader();
        glUniform2i(uniform, data.x, data.y);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, Vector2f data) {
        useShader();
        glUniform2f(uniform, data.x, data.y);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, Vector3i data) {
        useShader();
        glUniform3i(uniform, data.x, data.y, data.z);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, Vector3f data) {
        useShader();
        glUniform3f(uniform, data.x, data.y, data.z);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, Vector4i data) {
        useShader();
        glUniform4i(uniform, data.x, data.y, data.z, data.w);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, Vector4f data) {
        useShader();
        glUniform4f(uniform, data.x, data.y, data.z, data.w);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, Matrix3f data) {
        useShader();
        FloatBuffer fb;

        try (MemoryStack stack = MemoryStack.stackPush()) {
            fb = stack.mallocFloat(9);
        }

        data.get(fb);

        glUniformMatrix3fv(uniform, false, fb);
    }

    /**
     * Seta uma variavel uniforme.
     *
     * @param uniform   ID da variavel uniforme.
     * @param data     Novo valor da variavel.
     */
    public void setUniform(int uniform, Matrix4f data) {
        useShader();
        FloatBuffer fb;

        try (MemoryStack stack = MemoryStack.stackPush()) {
            fb = stack.mallocFloat(16);
        }

        data.get(fb);

        glUniformMatrix4fv(uniform, false, fb);
    }

    public void useShader() {
        shaderProgram.bind();
    }

    public void unbindShader() {
        shaderProgram.unbind();
    }

    public ShaderProgram getShaderProgram() {
        return shaderProgram;
    }
}
