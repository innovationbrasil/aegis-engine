package tk.awfulmint.aegisengine.opengl.shader;

/**
 * Essa classe representa um shader.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class Shader {
    private int shaderID;
    private String shaderFilePath;

    public Shader(int shaderID, String shaderFilePath) {
        this.shaderID = shaderID;
        this.shaderFilePath = shaderFilePath;
    }

    public int getShaderID() {
        return shaderID;
    }

    public String getShaderFilePath() {
        return shaderFilePath;
    }

}
