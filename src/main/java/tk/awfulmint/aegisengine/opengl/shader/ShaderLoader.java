package tk.awfulmint.aegisengine.opengl.shader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static org.lwjgl.opengl.GL20.*;

/**
 * Esta serve para carregar shaders apartir de arquivos.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class ShaderLoader {

    /**
     * Carrega um shader.
     * @param shaderPath    Caminho para o arquivo do shader.
     * @param shaderType    Tipo do shader a ser carregado.
     * @return  Um objecto do tipo Shader que pode ser usado para criar um programa.
     */
    public static Shader loadShader(String shaderPath, int shaderType) {
        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(shaderPath))) {
            String line;

            while((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int shaderID = glCreateShader(shaderType);
        glShaderSource(shaderID, sb);
        glCompileShader(shaderID);

        if(glGetShaderi(shaderID, GL_COMPILE_STATUS) == 0) {
            throw new RuntimeException("Erro enquanto compilando o shader! Error message:\n" + glGetShaderInfoLog(shaderID, 2048));
        }

        return new Shader(shaderID, shaderPath);
    }

}
