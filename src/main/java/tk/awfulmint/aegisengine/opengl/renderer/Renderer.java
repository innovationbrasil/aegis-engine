package tk.awfulmint.aegisengine.opengl.renderer;

import tk.awfulmint.aegisengine.opengl.entity.Entity;
import tk.awfulmint.aegisengine.opengl.model.RawMesh;
import tk.awfulmint.aegisengine.opengl.model.TexturedModel;
import tk.awfulmint.aegisengine.opengl.shader.StaticShader;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

/**
 * Esta classe é responsável pela renderização dos objetos.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class Renderer {
    private StaticShader shader;

    /**
     * Cria um novo Renderer.
     *
     * @param shader    StaticShader.
     */
    public Renderer(StaticShader shader) {
       this.shader = shader;
    }

    /**
     * Renderiza uma entidade.
     *
     * @param entity Uma entidade com textura, cores e com suporte a matrizes.
     */
    public void render(Entity entity) {
        TexturedModel texturedModel = entity.getTexturedModel();
        RawMesh rawMesh = texturedModel.getRawMesh();
        glBindVertexArray(rawMesh.getVaoID());
        glEnableVertexAttribArray(0);   //vertices
        glEnableVertexAttribArray(1);   //colors
        glEnableVertexAttribArray(2);   //texCords

        glActiveTexture(GL_TEXTURE0);
        shader.setTextureSampler(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texturedModel.getTexture().getTextureID());

        glDrawElements(GL_TRIANGLES, rawMesh.getVertexCount(), GL_UNSIGNED_INT, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
        glDisableVertexAttribArray(2);
        glBindVertexArray(0);
    }

}
