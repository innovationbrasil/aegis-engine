package tk.awfulmint.aegisengine.opengl.model;

import tk.awfulmint.aegisengine.opengl.texture.ModelTexture;
import tk.awfulmint.aegisengine.util.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

/**
 * Esta classe serve para carregar modelos em geral.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class ModelLoader {
    private ArrayList<Integer> vaos = new ArrayList<>();
    private ArrayList<Integer> vbos = new ArrayList<>();

    /**
     * Cria um RawMesh simples com os parametros especificados.
     *
     * @param vertexPositions Posição dos vertices
     * @param colors    Array de cores, cada 3 itens representao a cor de 1 unico vertice.
     * @param indicesArray Indices dos vertices
     * @return  Um RawMesh pronto para renderização.
     */
    public RawMesh loadMesh(float[] vertexPositions, float[] colors, int[] indicesArray) {
        int vao = createVAO();

        bindIndicesArray(indicesArray);
        bindVBO(0, 3, vertexPositions);
        bindVBO(1, 3, colors);

        unbindVAO();
        return new RawMesh(vao, indicesArray.length);
    }

    /**
     * Carrega um TexturedModel.
     *
     * @param mesh      Malha do objeto.
     * @param texture   Textura do objeto.
     * @param texCords  Coordenadas da textura.
     * @return  Um TexturedModel.
     */
    public TexturedModel loadModel(RawMesh mesh, ModelTexture texture, float[] texCords) {
        glBindVertexArray(mesh.getVaoID());
        bindVBO(2, 2, texCords);
        unbindVAO();

        return new TexturedModel(mesh, texture);
    }

    /**
     * Limpa o "lixo", no caso, os VAOs e VBOs gerados anteriormente.
     */
    public void garbageCleanUp() {
        for (Integer vao : vaos) glDeleteVertexArrays(vao);
        for (Integer vbo : vbos) glDeleteBuffers(vbo);
    }

    /**
     * Cria e binda um VBO ao VAO que estiver ativo no momento.
     *
     * @param index Posição da lista aonde o VBO irá ser colocado.
     * @param size  Tamanho das coordenadas dos dados. (1, 2, 3, 4).
     * @param data  Dados que a VBO irá guardar.
     */
    private void bindVBO(int index, int size, float[] data) {
        int vbo = glGenBuffers();
        vbos.add(vbo);
        FloatBuffer buffer = BufferUtils.createBuffer(data);

        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
        glVertexAttribPointer(index, size, GL_FLOAT, false, 0, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    /**
     * Binda uma array de indices ao VAO. Esta array apesar de ser
     * um VBO nao precisa do comando glVertexAttribPointer(...).
     *
     * @param indicesArray  Array de indices.
     */
    private void bindIndicesArray(int[] indicesArray) {
        IntBuffer buffer = BufferUtils.createBuffer(indicesArray);
        int vbo = glGenBuffers();
        vbos.add(vbo);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
    }

    /**
     * Cria e binda um VAO.
     *
     * @return ID do VAO.
     */
    private int createVAO() {
        int vao = glGenVertexArrays();
        vaos.add(vao);
        glBindVertexArray(vao);

        return vao;
    }

    /**
     * Desbinda um VAO, para não haver conflitos.
     */
    private void unbindVAO() {
        glBindVertexArray(0);
    }

}
