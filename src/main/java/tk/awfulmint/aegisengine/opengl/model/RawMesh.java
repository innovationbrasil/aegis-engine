package tk.awfulmint.aegisengine.opengl.model;

/**
 * Essa classe representa uma malha de modelo com
 * VAO basico.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class RawMesh {
    private int vaoID, vertexCount;

    /**
     * Cria um novo RawMesh.
     *
     * @param vaoID         ID do VAO da malha.
     * @param vertexCount   Contagem de vertices.
     */
    public RawMesh(int vaoID, int vertexCount) {
        this.vaoID = vaoID;
        this.vertexCount = vertexCount;
    }

    public int getVaoID() {
        return vaoID;
    }

    public int getVertexCount() {
        return vertexCount;
    }
}
