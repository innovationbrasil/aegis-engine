package tk.awfulmint.aegisengine.opengl.model;

import tk.awfulmint.aegisengine.opengl.texture.ModelTexture;

/**
 * Esta classe representa um modelo com textura.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class TexturedModel {
    private RawMesh rawMesh;
    private ModelTexture texture;

    /**
     * Cria um TexturedModel.
     *
     * @param rawMesh   Modelo base.
     * @param texture   Textura.
     */
    public TexturedModel(RawMesh rawMesh, ModelTexture texture) {
        this.rawMesh = rawMesh;
        this.texture = texture;
    }


    public RawMesh getRawMesh() {
        return rawMesh;
    }

    public void setRawMesh(RawMesh rawMesh) {
        this.rawMesh = rawMesh;
    }

    public ModelTexture getTexture() {
        return texture;
    }

    public void setTexture(ModelTexture texture) {
        this.texture = texture;
    }
}
