package tk.awfulmint.aegisengine.opengl;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.glClearColor;

public class Window {
    public static long winID;

    public static void createWindow(int width, int height, String name) {
        GLFWErrorCallback.createPrint(System.err).set();
        if(!glfwInit()) {
            throw new IllegalStateException("Nao foi possivel inicializar o GLFW!");
        }

        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        winID = glfwCreateWindow(width, height, name, 0L, 0L);
        if(winID == 0L) {
            System.out.println(winID);
            throw new IllegalStateException("Nao foi possivel criar uma janela GLFW");
        }

        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer w = stack.mallocInt(1);
            IntBuffer h = stack.mallocInt(1);

            glfwGetWindowSize(winID, w, h);
            GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            glfwSetWindowPos(
                    winID,
                    (vidMode.width() - w.get(0)) / 2,
                    (vidMode.height() - h.get(0)) / 2
            );
        }

        glfwMakeContextCurrent(winID);
        glfwSwapInterval(1);

        GL.createCapabilities();
        glClearColor(0.5f, 0.2f, 1.0f, 0.0f);
    }
}
