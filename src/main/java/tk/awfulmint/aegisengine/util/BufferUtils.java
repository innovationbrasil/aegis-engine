package tk.awfulmint.aegisengine.util;

import org.lwjgl.system.MemoryStack;

import java.nio.*;

/**
 * Varias funções para manipulação de Buffers.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public class BufferUtils {

    /**
     * Cria um FloatBuffer com a array fornecida.
     * @param data  Dados a serem passados para o buffer
     * @return  Um FloatBuffer com os dados contidos em 'data'.
     */
    public static FloatBuffer createBuffer(float[] data) {
        FloatBuffer fb;

        try (MemoryStack stack = MemoryStack.stackPush()) {
            fb = stack.mallocFloat(data.length);
            fb.put(data).flip();

            return fb;
        }
    }

    /**
     * Cria um IntBuffer com a array fornecida.
     * @param data  Dados a serem passados para o buffer
     * @return  Um IntBuffer com os dados contidos em 'data'.
     */
    public static IntBuffer createBuffer(int[] data) {
        IntBuffer ib;

        try (MemoryStack stack = MemoryStack.stackPush()) {
            ib = stack.callocInt(data.length);
            ib.put(data).flip();

            return ib;
        }
    }

}
