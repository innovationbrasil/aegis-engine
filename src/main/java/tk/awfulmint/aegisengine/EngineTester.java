package tk.awfulmint.aegisengine;

import tk.awfulmint.aegisengine.opengl.Window;
import tk.awfulmint.aegisengine.opengl.entity.Entity;
import tk.awfulmint.aegisengine.opengl.model.ModelLoader;
import tk.awfulmint.aegisengine.opengl.model.RawMesh;
import tk.awfulmint.aegisengine.opengl.model.TexturedModel;
import tk.awfulmint.aegisengine.opengl.texture.ModelTexture;
import tk.awfulmint.aegisengine.opengl.texture.TextureLoader;

import static org.lwjgl.glfw.GLFW.*;

public class EngineTester extends AEGISEngine {
    private Entity entity;

    public EngineTester(int wWidth, int wHeight, String wTitle) {
        super(wWidth, wHeight, wTitle);
    }

    public static void main(String[] args) {
        new EngineTester(1280, 720, "AEGIS Engine");
    }

    @Override
    public void EnginePreInit() {
        float[] vertices = {
                -0.5f, 0.5f, 0,
                -0.5f,-0.5f, 0,
                 0.5f,-0.5f, 0,
                 0.5f, 0.5f, 0
        };

        float[] colors = {
                1.0f, 0, 0,
                0, 1.0f, 0,
                0, 0, 1.0f,
                0, 1.0f, 1.0f
        };

        float[] texCords = {
                0,0,
                0,1,
                1,1,
                1,0
        };

        int[] indicesArray = {
                0, 1, 3,
                3, 2, 1
        };

        RawMesh mesh = modelLoader.loadMesh(vertices, colors, indicesArray);
        ModelTexture texture = TextureLoader.loadTexture("data/textures/teste.png");
        TexturedModel texturedModel = modelLoader.loadModel(mesh, texture, texCords);
        entity = entityLoader.loadEntity(texturedModel);
        entity.setScale(0.25f);
        addEntityToRenderQueue(entity);
    }

    @Override
    public void EngineInput() {
        glfwSetKeyCallback(Window.winID, (window, key, scancode, action, mods) -> {
            if(key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
                Terminate();
            }

            if(key == GLFW_KEY_F1 && action == GLFW_RELEASE) {
                staticShader.setRenderMode(0);
            }

            if(key == GLFW_KEY_F1 && action == GLFW_PRESS) {
                staticShader.setRenderMode(1);
            }

            if(key == GLFW_KEY_F2 && action == GLFW_PRESS) {
                staticShader.setRenderMode(2);
            }

            if(key == GLFW_KEY_F2 && action == GLFW_RELEASE) {
                staticShader.setRenderMode(0);
            }

            if(key == GLFW_KEY_A) {
                mainCamera.updatePos(0.1f, 0, 0);
            } else if(key == GLFW_KEY_D) {
                mainCamera.updatePos(-0.1f, 0, 0);
            } else if(key == GLFW_KEY_Q) {
                mainCamera.updateRotation(0, 0, 1.25f);
            } else if(key == GLFW_KEY_E) {
                mainCamera.updateRotation(0, 0, -1.25f);
            } else if(key == GLFW_KEY_W) {
                mainCamera.updatePos(0, 0, 1f);
            } else if(key == GLFW_KEY_S) {
                mainCamera.updatePos(0, 0, -1f);
            } else if(key == GLFW_KEY_SPACE) {
                mainCamera.updatePos(0, -1f, 0);
            } else if(key == GLFW_KEY_LEFT_CONTROL) {
                mainCamera.updatePos(0, 1f, 0);
            }
        });
    }
}
