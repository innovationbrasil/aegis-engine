package tk.awfulmint.aegisengine;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import tk.awfulmint.aegisengine.opengl.Window;
import tk.awfulmint.aegisengine.opengl.entity.Camera;
import tk.awfulmint.aegisengine.opengl.entity.Entity;
import tk.awfulmint.aegisengine.opengl.entity.EntityLoader;
import tk.awfulmint.aegisengine.opengl.model.ModelLoader;
import tk.awfulmint.aegisengine.opengl.model.RawMesh;
import tk.awfulmint.aegisengine.opengl.model.TexturedModel;
import tk.awfulmint.aegisengine.opengl.renderer.Renderer;
import tk.awfulmint.aegisengine.opengl.shader.StaticShader;

import java.util.ArrayList;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

/**
 * Classe principal da engine! Este construtor inicializa
 * a engine e o jogo.
 *
 * Contém campos publicos para os módulos da engine. (Ex: ModelLoader).
 * Para um campo poder ser público ele tem que ser seguro, que não faça
 * chamadas criticas do OpenGL ou chamadas internas dos módulos.
 *
 * @author PCXDO
 * @version v0.1
 * @since v0.1
 */
public abstract class AEGISEngine {
    private Renderer renderer;
    private ArrayList<Entity> renderQueue = new ArrayList<>();

    public ModelLoader modelLoader;
    public EntityLoader entityLoader;
    public StaticShader staticShader;
    public Camera mainCamera;

    public static float winWidth, winHeight;

    /**
     * Este metodo serve para carregamento e inicialização
     * de recursos do jogo.
     */
    public abstract void EnginePreInit();

    /**
     * Este metodo serve somente para criar um KeyCallback
     * sinples.
     */
    public abstract void EngineInput();

    /**
     * Cria uma instancia da Engine. Esse construtor atualmente
     * inicializa a engine em si. Cria o contexto do OpenGL e
     * do GLFW assim como registra modelos na lista de renderização
     * e chama o método EngineLoop.
     *
     * @param wWidth    Largura da janela
     * @param wHeight   Altura da janela
     * @param wTitle    Nome da janela
     */
    public AEGISEngine(int wWidth, int wHeight, String wTitle) {
        //Setup do GLFW
        Window.createWindow(wWidth, wHeight, wTitle);
        winHeight = (float) wHeight;
        winWidth = (float) wWidth;
        System.out.println(Window.winID);
        EngineInit();
    }

    /**
     * Adiciona uma entidade a lista de renderização.
     *
     * @param entity   Entidade a ser adicionado na lista.
     */
    public void addEntityToRenderQueue(Entity entity) {
        renderQueue.add(entity);
    }

    /**
     * Inicializa a engine e chama pelo EngineLoop.
     */
    private void EngineInit() {
        staticShader = new StaticShader().createNew();
        renderer = new Renderer(staticShader);
        modelLoader = new ModelLoader();
        entityLoader = new EntityLoader();
        mainCamera = entityLoader.loadCamera(new Vector3f(0,0,0), new Vector3f(0,0,180), staticShader);

        EnginePreInit(); //Carrega modelos, texturas, sons etc.
        EngineInput(); //Este nao é um meotodo de update e só serve pra setar o Callback.

        EngineLoop();
    }

    /**
     * Loop principal da engine! Aqui é onde ocorre a mágica.
     */
    private void EngineLoop() {
        while(!glfwWindowShouldClose(Window.winID)) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glfwPollEvents();

            for (Entity entity : renderQueue) {
                staticShader.useShader();

                staticShader.setupViewMatrix(staticShader.getViewMatrix(mainCamera));
                staticShader.setupModelMatrix(staticShader.getModelMatrix(entity));

                renderer.render(entity);

                staticShader.unbindShader();
            }

            glfwSwapBuffers(Window.winID);
        }

        assert modelLoader != null;
        modelLoader.garbageCleanUp();
        staticShader.getShaderProgram().clean();

        glfwTerminate();
    }

    /**
     * Função para saida emergencial da Engine. Esta função
     * pode gerar acumulo de lixo na Heap.
     */
    public void Terminate() {
        System.exit(-1);
    }

}
