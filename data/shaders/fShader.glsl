#version 400 core

in vec3 passColor;
in vec2 passTexCords;

out vec4 result;

//Others
uniform int renderMode = 0;
uniform sampler2D texSampler;

void main() {
    if (renderMode == 0) {
        result = vec4(passColor, 1.0);
    } else if (renderMode == 1) {
        vec4 texColor = texture(texSampler, passTexCords);
        result = texColor;
    } else if (renderMode == 2) {
        vec4 texColor = texture(texSampler, passTexCords);
        result = vec4(passColor, 1.0) + texColor;
    }
}
