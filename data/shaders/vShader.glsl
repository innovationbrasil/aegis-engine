#version 400 core

layout (location = 0) in vec3 vertPos;
layout (location = 1) in vec3 colors;
layout (location = 2) in vec2 texCords;

out vec3 passColor;
out vec2 passTexCords;

//Matrices
uniform mat4 projMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;

void main() {
    gl_Position = projMatrix * modelMatrix * viewMatrix * vec4(vertPos, 1.0);
    passColor = colors;
    passTexCords = texCords;
}
