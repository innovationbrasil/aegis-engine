# AEGIS Game Engine #
[ ![Codeship Status for innovationbrasil/aegis-engine][CodeShipLink]][CodeShipCheck]
---

Esta engine é um rework de um antigo projeto meu chamado Mint Engine que
originalmente era em C++. Agora será em Java, a Engine será inteiramente
focada em OpenGL. Apesar de eu ter planos de fazer implementações de
Vulkan.

Atualizações, testes e devlogs serão conduzidos no meu [canal do YouTube], 
de uma conferida mais tarde pra dar aquela força ;)

## Features:
* Multiplos modos de renderização. (Fragment Shader).
* Espaço 3D completo.
* View, Model e Projection Matrix.
* Lista de renderização. (Em breve sistema de prioridade).
* Wireframe View (Em breve).

## Screenshots
`Nenhuma por enquanto ;-;`

## Como contribuir?
A parte mais importante do desenvolvimento é garantir a qualidade então,
testando e enviando seu feedback e relatando bugs ajuda muito.

Você ainda pode ajudar consertando bugs, enviando assets, idéias
ou fazendo uma doação :D

## Contato para falar comigo
Eu passo a maior parte online com o meu discord aberto. Basta clicar 
neste link: https://discord.gg/FEDtw

Dizem por aí que é possível me encontrar na steam como AwfulMint. :)

## Licensa
Este repositório, assim como todos os mesmo do AEGIS Project estão protegidos
sobre a licensa Creative Commons 4.0 BY-NC-ND.
[![Licença Creative Commons][CCLogo]][CCLink]

[//]: # (Area de links extras do Markdown)
[canal do YouTube]: https://www.youtube.com/channel/UC68gTeVRfGzcdXUV1X_I1uQ
[CCLogo]: https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png
[CCLink]: http://creativecommons.org/licenses/by-nc-nd/4.0/
[CodeShipLink]: https://app.codeship.com/projects/b7c744d0-2043-0136-4d2c-521ef473b41a/status?branch=master
[CodeShipCheck]: https://app.codeship.com/projects/285612